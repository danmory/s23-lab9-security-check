# Security check

Site: hh.ru

## Authorization

### Ensure Lookup IDs are Not Accessible Even When Guessed or Cannot Be Tampered With

<https://cheatsheetseries.owasp.org/cheatsheets/Authorization_Cheat_Sheet.html#ensure-lookup-ids-are-not-accessible-even-when-guessed-or-cannot-be-tampered-with>

| Test step                                                                                  | Result                                        |
|--------------------------------------------------------------------------------------------|-----------------------------------------------|
| Open hh.ru                                                                                 | Ok                                            |
| Click on avatar icon in Navigation bar and open your profile                               | Ok                                            |
| Check url if it contains some identifiers                                                  | Ok, no identifiers are exposed                |
| Click "Resumes" in Navigation bar                                                          | Ok                                            |
| Open any resume                                                                            | Ok                                            |
| Check url if it contains some identifiers                                                  | Not ok, resume ID is exposed                  |
| If any ID is exposed check if unauthorized user can access the resource via ID | Ok, other users can not open the resume by ID |

**Comment:**

hh.ru exposes the resume ID which is not the best solution according to OWASP CSS.
However, this is not a critical security issue because:

1. If unauthorized user tries to access the resume then the system rejects the request. Moreover, there is no differences between error messages, when "hacker" attempts to read existing and non-existing resumes.
2. The resume owner can control whether resume is accessible via ID or not. So, it is convenient way to allow or deny access to the resume.

**Conclusion:**

No problems with this security aspect as there are authorization mechanisms.

**The error message when attempting to check resume via ID(both existing and non-existing):**

![out](authorization.png)

## File Upload

### Extension Validation(+ Content-Type Validation)

<https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html#extension-validation>

| Test step                                                                                       | Result            |
|-------------------------------------------------------------------------------------------------|-------------------|
| Open hh.ru                                                                                      | Ok                |
| Click on avatar icon in Navigation bar and open your profile                                    | Ok                |
| Click "Images"                                                                                  | Ok                |
| Click "Upload Photo"                                                                            | Ok                |
| Check allowed extensions                                                                        | Ok: jpg, png, bmp |
| Try to upload file with allowed extension                                                       | Ok, success       |
| Try to upload file with unallowed extension                                                     | Ok, not allowed   |
| Try to upload incorrect file with allowed extension(change extension from unallowed to allowed) | Ok, not allowed   |

**Comment:**

hh.ru fully satisfies the specified security criteria for uploading files. It checks extension and MIME types. So, there should be no errors.

**Conclusion:**

Secure, no problems.

**Screenshot with validation result of incorrect files:**

![out](file_upload.png)

## Authentication

### Implement Proper Password Strength Controls

<https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#implement-proper-password-strength-controls>

| Test step                                                | Result                                   |
|----------------------------------------------------------|------------------------------------------|
| Open hh.ru                                               | Ok                                       |
| Click on avatar icon in Navigation bar and open settings | Ok                                       |
| Click "Set a password"                                   | Ok                                       |
| Write short password(< 8 symbols)                        | Not ok, allowed to use 6 symbols         |
| Write password with whitespaces - they should remain     | Not ok, whitespaces are truncated        |
| Write password with unicode characters and whitespaces   | Not ok, these characters are not allowed |

**Comment:**

hh.ru allows weak passwords which may introduce security risks.

**Conclusion:**

Insecure, criteria for passwords should be strengten.

**Screenshot of validation:**

Does not allow unicode characters and allows < 8 symbols.

![out](password.png)

## Input Validation

### Email Address Validation

<https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html#email-address-validation>

| Test step                                             | Result                                                                  |
|-------------------------------------------------------|-------------------------------------------------------------------------|
| Open hh.ru                                            | Ok                                                                      |
| Click "Create a resume"                               | Ok                                                                      |
| Enter correct email                                   | Ok                                                                      |
| Enter email without @                                 | Ok, validation is correct                                               |
| Enter email with dangerous characters(e.g. backslash) | Ok, validation is correct                                               |
| Enter local part (before the @) > 63 symbols          | Not ok, validation is incorrect                                         |
| Enter email > 256 symbols                             | Ok, 127 symbols is max(even if I change maxlength HTML input attribute) |

**Comment:**

hh.ru validates email correctly except length of the local part. However, it checks whether email really exists as it sends verification letter at registration stage. So, there should be no errors.

**Conclusion:**

Secure, no problems.

**Screenshot of validation:**

Correctly validates:

![out](email.png)

Incorrectly validates:

![out](invalid_email.png)
